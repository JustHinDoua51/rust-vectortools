# VectorTools

This library offers a comprehensive set of tools for manipulating vectors in Rust. Whether you're working on image processing, scientific computing, or game algorithms, this library provides powerful features for handling and analyzing vectors with ease.

## Authors

- [@JustHinDoua51](https://gitlab.com/JustHinDoua51)

## Installation

To use this library in your Rust project, simply add the following line to your Cargo.toml file:

```bash
[dependencies]
vectors = "0.1.0"
```
    
## Documentation

[Documentation](https://justhindoua69.github.io/doc/VectorTools/)

## Features

- **Vector Operations** : Add, subtract, multiply, and divide vectors effortlessly.
- **Advanced Calculations** : Compute norms, dot products, and cross products in a snap.
- **Geometric Transformations** : Perform translations, rotations, and scaling on vectors with precision.
- **Rust Compatibility** : Seamlessly integrate with the Rust language for smooth usage in your projects.

## Usage/Examples

```rust
fn main() {
    let m1 = Matrix::new(vec![
        Vector::new(vec![1.0, 2.0, 3.0, 4.0, 5.0]),
        Vector::new(vec![6.0, 7.0, 8.0, 9.0, 10.0]),
        Vector::new(vec![11.0, 12.0, 13.0, 14.0, 15.0]),
        Vector::new(vec![16.0, 17.0, 18.0, 19.0, 20.0]),
        Vector::new(vec![21.0, 22.0, 23.0, 24.0, 25.0]),
    ]);

    let m2 = Matrix::new(vec![
        Vector::new(vec![1.0, 2.0, 3.0, 4.0, 5.0]),
        Vector::new(vec![6.0, 7.0, 8.0, 9.0, 10.0]),
        Vector::new(vec![11.0, 12.0, 13.0, 14.0, 15.0]),
        Vector::new(vec![16.0, 17.0, 18.0, 19.0, 20.0]),
        Vector::new(vec![21.0, 22.0, 23.0, 24.0, 25.0]),
    ]);

    let result = &m1 * &m2;
    println!("{}", result);
}
```