use std::fmt;
use std::ops::{Add, Sub, Mul, Div};

/// A n-dimensional vector struct with x, y, z and ... components.
#[derive(Debug, Clone)]
pub struct Vector {
    pub components: Vec<f64>,
}

impl Vector {
    /// Creates a new Vector with the specified x, y, and z components.
    ///
    /// # Arguments
    ///
    /// * `x` - The x component of the vector.
    /// * `y` - The y component of the vector.
    /// * `z` - The z component of the vector.
    /// * `components` - The vector components.
    ///
    /// # Example
    ///
    /// ```
    /// let v1 = Vector::new(vec![1.0, 2.0, 3.0, 4.0]);
    /// let v2 = Vector::new(vec![1.0, 2.0, 4.0]);
    /// ```
    pub fn new(components: Vec<f64>) -> Self {
        Vector { components }
    }

    /// Calculates the dot product of this vector and another vector.
    ///
    /// # Arguments
    ///
    /// * `other` - The other vector.
    ///
    /// # Returns
    ///
    /// The dot product of the two vectors.
    ///
    /// # Example
    ///
    /// ```
    /// let v1 = Vector::new(vec![1.0, 2.0, 3.0]);
    /// let v2 = Vector::new(vec![1.0, 2.0, 4.0]);
    /// let dot_product = v1.dot(&v2);
    /// ```
    pub fn dot(&self, other: &Vector) -> f64 {
        if self.dimensions() != other.dimensions() {
            panic!("Vectors must have the same number of components for dot product calculation.");
        }
        self.components.iter().zip(&other.components).map(|(a, b)| a * b).sum()
    }

    /// Calculates the magnitude (length) of this vector.
    ///
    /// # Returns
    ///
    /// The magnitude of the vector.
    ///
    /// # Example
    ///
    /// ```
    /// let v = Vector::new(vec![1.0, 2.0, 3.0]);
    /// let magnitude = v.magnitude();
    /// ```
    pub fn magnitude(&self) -> f64 {
        self.components.iter().map(|&x| x * x).sum::<f64>().sqrt()
    }

    /// Normalizes this vector, producing a new vector with the same direction but a magnitude of 1.
    ///
    /// # Returns
    ///
    /// The normalized vector.
    ///
    /// # Example
    ///
    /// ```
    /// let v = Vector::new(vec![1.0, 2.0, 3.0]);
    /// let normalized_vector = v.normalize();
    /// ```
    pub fn normalize(&self) -> Vector {
        let mag = self.magnitude();
        Vector::new(self.components.iter().map(|&x| x / mag).collect())
    }

    /// Calculates the angle in radians between this vector and another vector.
    ///
    /// # Arguments
    ///
    /// * `other` - The other vector.
    ///
    /// # Returns
    ///
    /// The angle between the two vectors in radians.
    ///
    /// # Example
    ///
    /// ```
    /// let v1 = Vector::new(vec![1.0, 2.0, 3.0]);
    /// let v2 = Vector::new(vec![1.0, 2.0, 4.0]);
    /// let angle = v1.angle_between(&v2);
    /// ```
    pub fn angle_between(&self, other: &Vector) -> f64 {
        if self.dimensions() != other.dimensions() {
            panic!("Vectors must have the same number of components for angle calculation.");
        }
        let self_normalized = self.normalize();
        let other_normalized = other.normalize();
        let dot_product = self_normalized.dot(&other_normalized);

        if dot_product <= -1.0 {
            return std::f64::consts::PI;
        } else if dot_product >= 1.0 {
            return 0.0;
        }

        dot_product.acos()
    }

    /// Calculates the angle in degrees between this vector and another vector.
    ///
    /// # Arguments
    ///
    /// * `other` - The other vector.
    ///
    /// # Returns
    ///
    /// The angle between the two vectors in degrees.
    ///
    /// # Example
    ///
    /// ```
    /// let v1 = Vector::new(vec![1.0, 2.0, 3.0]);
    /// let v2 = Vector::new(vec![1.0, 2.0, 4.0]);
    /// let angle_degrees = v1.angle_between_degrees(&v2);
    /// ```
    pub fn angle_between_degrees(&self, other: &Vector) -> f64 {
        let angle_radians = self.angle_between(other);
        angle_radians.to_degrees()
    }

    /// Calculates the scalar projection of this vector onto another vector.
    ///
    /// # Arguments
    ///
    /// * `onto` - The vector onto which to project this vector.
    ///
    /// # Returns
    ///
    /// The scalar projection of this vector onto the other vector.
    ///
    /// # Example
    ///
    /// ```
    /// let v1 = Vector::new(vec![1.0, 2.0, 3.0]);
    /// let v2 = Vector::new(vec![1.0, 2.0, 4.0]);
    /// let scalar_projection = v1.scalar_proj(&v2);
    /// ```
    pub fn scalar_proj(&self, onto: &Vector) -> f64 {
        if self.dimensions() != onto.dimensions() {
            panic!("Vectors must have the same number of components for scalar projection calculation.");
        }
        self.dot(onto) / onto.magnitude()
    }

    /// Projects this vector onto another vector.
    ///
    /// # Arguments
    ///
    /// * `other` - The vector onto which to project this vector.
    ///
    /// # Returns
    ///
    /// The projected vector.
    ///
    /// # Example
    ///
    /// ```
    /// let v1 = Vector::new(vec![1.0, 2.0, 3.0]);
    /// let v2 = Vector::new(vec![1.0, 2.0, 4.0]);
    /// let projected_vector = v1.project_onto(&v2);
    /// ```
    pub fn project_onto(&self, other: &Vector) -> Vector {
        if self.dimensions() != other.dimensions() {
            panic!("Vectors must have the same number of components for vector projection calculation.");
        }
        let scalar = self.dot(other) / other.dot(other);
        other.clone() * scalar
    }

    /// Converts radians to degrees.
    ///
    /// # Arguments
    ///
    /// * `radians` - The angle in radians.
    ///
    /// # Returns
    ///
    /// The angle in degrees.
    ///
    /// # Example
    ///
    /// ```
    /// let radians = std::f64::consts::PI;
    /// let degrees = Vector::to_degrees(radians);
    /// ```
    pub fn to_degrees(radians: f64) -> f64 {
        radians * 180.0 / std::f64::consts::PI
    }

    /// Converts degrees to radians.
    ///
    /// # Arguments
    ///
    /// * `degrees` - The angle in degrees.
    ///
    /// # Returns
    ///
    /// The angle in radians.
    ///
    /// # Example
    ///
    /// ```
    /// let degrees = 180.0;
    /// let radians = Vector::to_radians(degrees);
    /// ```
    pub fn to_radians(degrees: f64) -> f64 {
        degrees * std::f64::consts::PI / 180.0
    }

    /// Calculates the orthogonal projection of this vector onto another vector.
    ///
    /// # Arguments
    ///
    /// * `onto` - The vector onto which to project this vector orthogonally.
    ///
    /// # Returns
    ///
    /// The orthogonal projection of this vector onto the other vector.
    ///
    /// # Example
    ///
    /// ```
    /// let v1 = Vector::new(vec![1.0, 2.0, 3.0]);
    /// let v2 = Vector::new(vec![1.0, 2.0, 4.0]);
    /// let orthogonal_projection = v1.orthogonal_proj(&v2);
    /// ```
    pub fn orthogonal_proj(&self, onto: &Vector) -> Vector {
        if self.dimensions() != onto.dimensions() {
            panic!("Vectors must have the same number of components for orthogonal projection calculation.");
        }
        let vector_proj = self.project_onto(onto);
        self.clone() - vector_proj
    }

    /// Calculates the angle in radians between this vector and the n-axis.
    ///
    /// # Returns
    ///
    /// The angle between the vector and the n-axis in radians.
    ///
    /// # Example
    ///
    /// ```
    /// let v = Vector::new(vec![1.0, 2.0, 3.0]);
    /// let angle = v.angle_with_x_axis();
    /// ```
    pub fn angle_with_axis(&self, axis: &Vector) -> f64 {
        if self.dimensions() != axis.dimensions() {
            panic!("Vectors must have the same number of components for angle calculation.");
        }
        self.angle_between(axis)
    }    

    /// Calculates the dimensions of this vector.
    ///
    /// # Returns
    ///
    /// The number of components in this vector.
    ///
    /// # Example
    ///
    /// ```
    /// let v = Vector::new(vec![1.0, 2.0, 3.0]);
    /// let dimensions = v.dimensions();
    /// ```
    pub fn dimensions(&self) -> usize {
        self.components.len()
    }
}

impl fmt::Display for Vector {
    /// Displays this vector as a string.
    ///
    /// # Returns
    ///
    /// This vector as a string.
    ///
    /// # Example
    ///
    /// ```
    /// let v = Vector::new(vec![1.0, 2.0, 3.0]);
    /// println!("Vector: {}", v);
    /// ```
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "(")?;
        for (index, component) in self.components.iter().enumerate() {
            if index > 0 {
                write!(f, ", ")?;
            }
            write!(f, "{}", component)?;
        }
        write!(f, ")")
    }
}

/// Implements the addition operation for Vector.
///
/// # Examples
///
/// ```
/// let v1 = Vector::new(vec![1.0, 2.0, 3.0]);
/// let v2 = Vector::new(vec![1.0, 2.0, 4.0]);
/// let sum = v1 + v2;
/// ```
impl Add for Vector {
    type Output = Vector;

    fn add(self, other: Vector) -> Vector {
        if self.dimensions() != other.dimensions() {
            panic!("Vectors must have the same number of components for addition.");
        }
        let components = self.components.iter().zip(other.components.iter()).map(|(&a, &b)| a + b).collect();
        Vector::new(components)
    }
}

/// Implements the subtraction operation for Vector.
///
/// # Examples
///
/// ```
/// let v1 = Vector::new(vec![1.0, 2.0, 3.0]);
/// let v2 = Vector::new(vec![1.0, 2.0, 4.0]);
/// let sum = v1 + v2;
/// ```
impl Sub for Vector {
    type Output = Vector;

    fn sub(self, other: Vector) -> Vector {
        if self.dimensions() != other.dimensions() {
            panic!("Vectors must have the same number of components for subtraction.");
        }
        let components = self.components.iter().zip(other.components.iter()).map(|(&a, &b)| a - b).collect();
        Vector::new(components)
    }
}

/// Implements scalar multiplication operation for Vector.
///
/// # Examples
///
/// ```
/// let v = Vector::new(vec![1.0, 2.0, 3.0]);
/// let scaled = v * 2.0;
/// ```
impl Mul<f64> for Vector {
    type Output = Vector;

    fn mul(self, scalar: f64) -> Vector {
        let components = self.components.iter().map(|&x| x * scalar).collect();
        Vector::new(components)
    }
}

/// Implements scalar division operation for Vector.
///
/// # Examples
///
/// ```
/// let v = Vector::new(vec![1.0, 2.0, 3.0]);
/// let scaled = v / 2.0;
/// ```
impl Div<f64> for Vector {
    type Output = Vector;

    fn div(self, scalar: f64) -> Vector {
        if scalar == 0.0 {
            panic!("Division by zero is not allowed.");
        }
        let components = self.components.iter().map(|&x| x / scalar).collect();
        Vector::new(components)
    }
}


/// Implements component-wise multiplication operation for Vector.
///
/// # Examples
///
/// ```
/// let v1 = Vector::new(vec![1.0, 2.0, 3.0]);
/// let v2 = Vector::new(vec![1.0, 2.0, 4.0]);
/// let product = v1 * v2;
/// ```
impl Mul<Vector> for Vector {
    type Output = Vector;

    fn mul(self, other: Vector) -> Vector {
        if self.dimensions() != other.dimensions() {
            panic!("Vectors must have the same number of components for component-wise multiplication.");
        }
        let components = self.components.iter().zip(other.components.iter())
            .map(|(&a, &b)| a * b)
            .collect();
        Vector::new(components)
    }
}

/// Implements component-wise division operation for Vector.
///
/// # Examples
///
/// ```
/// let v1 = Vector::new(vec![1.0, 2.0, 3.0]);
/// let v2 = Vector::new(vec![1.0, 2.0, 4.0]);
/// let product = v1 * v2;
/// ```
impl Div<Vector> for Vector {
    type Output = Vector;

    fn div(self, other: Vector) -> Vector {
        if self.dimensions() != other.dimensions() {
            panic!("Vectors must have the same number of components for component-wise division.");
        }
        let components = self.components.iter().zip(other.components.iter())
            .map(|(&a, &b)| {
                if b == 0.0 {
                    panic!("Division by zero is not allowed.");
                }
                a / b
            })
            .collect();
        Vector::new(components)
    }
}

/// A n-dimensional matrix struct with vectors components.
pub struct Matrix {
    pub rows: Vec<Vector>,
}

impl Matrix {
    /// Creates a new Matrix with the specified vectors components.
    ///
    /// # Arguments
    ///
    /// * `components` - The vector components of the Matrix.
    ///
    /// # Example
    ///
    /// ```
    /// let m = Matrix::new(vec![
    /// Vector::new(vec![1.0, 2.0]),
    /// Vector::new(vec![3.0, 4.0]),
    /// ]);
    /// ```
    pub fn new(rows: Vec<Vector>) -> Self {
        Matrix { rows }
    }

    /// Returns the number of rows in this matrix.
    ///
    /// # Returns
    ///
    /// The number of rows in this matrix.
    ///
    /// # Example
    ///
    /// ```
    /// let m = Matrix::new(vec![
    /// Vector::new(vec![1.0, 2.0]),
    /// Vector::new(vec![3.0, 4.0]),
    /// ]);
    /// assert_eq!(m.rows(), 2);
    /// ```
    pub fn rows(&self) -> usize {
        self.rows.len()
    }

    /// Returns the number of columns in this matrix.
    ///
    /// # Returns
    ///
    /// The number of columns in this matrix.
    ///
    /// # Example
    ///
    /// ```
    /// let m = Matrix::new(vec![
    /// Vector::new(vec![1.0, 2.0]),
    /// Vector::new(vec![3.0, 4.0]),
    /// ]);
    /// assert_eq!(m.cols(), 2);
    /// ```
    pub fn cols(&self) -> usize {
        if self.rows.is_empty() {
            0
        } else {
            self.rows[0].dimensions()
        }
    }
}

impl fmt::Display for Matrix {
    /// Displays this matrix as a string.
    ///
    /// # Returns
    ///
    /// This matrix as a string.
    ///
    /// # Example
    ///
    /// ```
    /// let m = Matrix::new(vec![
        /// Vector::new(vec![1.0, 2.0]),
        /// Vector::new(vec![3.0, 4.0]),
        /// ]);
    /// println!("Vector: {}", m);
    /// ```
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for row in &self.rows {
            writeln!(f, "{}", row)?;
        }
        Ok(())
    }
}

/// Implements multiplication operation for Matrices.
///
/// # Examples
///
/// ```
/// let m1 = Matrix::new(vec![
/// Vector::new(vec![1.0, 2.0]),
/// Vector::new(vec![3.0, 4.0]),
/// ]);
/// let m2 = Matrix::new(vec![
/// Vector::new(vec![1.0, 2.0]),
/// Vector::new(vec![3.0, 4.0]),
/// ]);
/// let product = m1 * m2;
/// ```
impl Mul<&Matrix> for &Matrix {
    type Output = Matrix;

    fn mul(self, other: &Matrix) -> Matrix {
        if self.cols() != other.rows() {
            panic!("Matrix dimensions mismatch for multiplication.");
        }

        let mut result = vec![];

        for i in 0..self.rows() {
            let mut new_row = vec![];
            for j in 0..other.cols() {
                let mut sum = 0.0;
                for k in 0..self.cols() {
                    sum += self.rows[i].components[k] * other.rows[k].components[j];
                }
                new_row.push(sum);
            }
            result.push(Vector::new(new_row));
        }

        Matrix::new(result)
    }
}


/// Implements multiplication operation for Matrices.
///
/// # Examples
///
/// ```
/// let m = Matrix::new(vec![
/// Vector::new(vec![1.0, 2.0]),
/// Vector::new(vec![3.0, 4.0]),
/// ]);
/// let v = Vector::new(vec![1.0, 2.0]);
/// let product = m * v;
/// ```
impl Mul<&Vector> for &Matrix {
    type Output = Vector;

    fn mul(self, vector: &Vector) -> Vector {
        if self.cols() != vector.dimensions() {
            panic!("Matrix and vector dimensions mismatch for multiplication.");
        }

        let mut result = vec![];

        for row in self.rows.iter() {
            let mut sum = 0.0;
            for (index, val) in row.components.iter().enumerate() {
                sum += val * vector.components[index];
            }
            result.push(sum);
        }

        Vector::new(result)
    }
}

/// Implements addition operation for Matrices.
///
/// # Examples
///
/// ```
/// let m1 = Matrix::new(vec![
/// Vector::new(vec![1.0, 2.0]),
/// Vector::new(vec![3.0, 4.0]),
/// ]);
/// let m2 = Matrix::new(vec![
/// Vector::new(vec![1.0, 2.0]),
/// Vector::new(vec![3.0, 4.0]),
/// ]);
/// let product = m1 + m2;
/// ```
impl Add<&Matrix> for &Matrix {
    type Output = Matrix;

    fn add(self, other: &Matrix) -> Matrix {
        if self.rows() != other.rows() || self.cols() != other.cols() {
            panic!("Matrix dimensions mismatch for addition.");
        }

        let mut result = vec![];

        for (row1, row2) in self.rows.iter().zip(other.rows.iter()) {
            let new_row = row1.clone() + row2.clone();
            result.push(new_row);
        }

        Matrix::new(result)
    }
}